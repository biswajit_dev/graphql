const { buildSchema } = require("graphql");

module.exports = buildSchema(`
    type Events {
        _id: ID!
        title: String!
        description: String!
        cost: Float!
        date: String!
        creator: User!
    }

    type User {
        _id: ID!
        email: String!
        password: String!
        createdEvents: [Events!]
    }

    type Booking {
        _id: ID!
        event: Events!
        user: User!
        createdAt: String!
        updatedAt: String!
    }

    type AuthData {
        userId: ID!
        token: String!
        tokenExpiration: Int!
    }

    input EventInput {
        title: String!
        description: String!
        cost: Float!
        date: String!
    }

    input UserInput {
        email: String!,
        password: String!
    }

    type RootQuery {
        events: [Events!]!
        booking: [Booking!]!
        login(email: String!, password: String!): AuthData!
    }

    type RootMutation {
        createEvent(eventInputs: EventInput): Events
        createUser(userInputs: UserInput): User
        bookEvent(eventID: ID!): Booking!
        cancelBooking(bookingId: ID!): Events
    }

    schema {
        query: RootQuery
        mutation: RootMutation
    }
`);
