const Event = require("../../model/event");
const User = require("../../model/user");

module.exports = {
  events: () => {
    return Event.find()
      .populate("creator")
      .then((result) => {
        return result;
      })
      .catch((err) => {
        throw err;
      });
  },
  createEvent: ({ eventInputs: { title, description, cost, date } }, req) => {
    if(!req.isAuth) {
      throw new Error("Unauthorize!");
    }
    const { userId } = req.user;

    const event = new Event({
      title: title,
      description: description,
      cost: cost,
      date: new Date(),
      creator: userId,
    });
    let createdEvent;
    return event
      .save()
      .then((result) => {
        createdEvent = result;
        return User.findById(userId);
      })
      .then((user) => {
        if (typeof user === "undefined" || !user) {
          throw new Error("User not exists.");
        }
        user.createdEvents = event;
        createdEvent.creator = user;
        return user.save();
      })
      .then((result) => {
        return createdEvent;
      })
      .catch((err) => {
        throw err;
      });
  },
};
