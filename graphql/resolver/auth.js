const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../../model/user");

module.exports = {
  createUser: ({ userInputs: { email, password } }) => {
    return User.findOne({ email })
      .then((user) => {
        if (user) {
          throw new Error("User already exists.");
        }
        return bcrypt.hash(password, 10);
      })
      .then((hashPassword) => {
        const user = new User({
          email,
          password: hashPassword,
        });

        return user
          .save()
          .then((result) => {
            return result;
          })
          .catch((err) => {
            throw err;
          });
      });
  },
  login: ({ email, password }) => {
    let userData;
    return User.findOne({ email })
      .then((user) => {
        if (!user) {
          throw new Error("User does not exists!");
        }
        userData = user;
        return bcrypt.compare(password, user.password);
      })
      .then((isEqual) => {
        if (!isEqual) {
          throw new Error("Password is wrong!");
        }
        const token = jwt.sign(
          { email: userData.email, userId: userData._id },
          process.env.TOKEN_SECRECT,
          {
            expiresIn: "1h",
          }
        );
        return {
          userId: userData._id,
          token,
          tokenExpiration: 1,
        };
      });
  },
};
