const Booking = require("../../model/booking");

module.exports = {
  booking: () => {
    return Booking.find()
      .populate("event")
      .populate("user")
      .then((bookings) => {
        return bookings;
      })
      .catch((err) => {
        throw err;
      });
  },

  bookEvent: ({ eventID }) => {
    if(!req.isAuth) {
      throw new Error("Unauthorize!");
    }
    const { userId } = req.user;

    return Event.findById(eventID)
      .then((event) => {
        const newBookingEvent = new Booking({
          event,
          user: [userId],
        });
        return newBookingEvent.save();
      })
      .then((result) => {
        return result;
      })
      .catch((err) => {
        throw err;
      });
  },

  cancelBooking: ({ bookingId }) => {
    return Booking.findByIdAndRemove(bookingId)
      .then((booking) => {
        return booking;
      })
      .catch((err) => {
        throw err;
      });
  },
};
