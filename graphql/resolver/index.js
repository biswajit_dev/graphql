const events = require("./events");
const user = require("./auth");
const booking = require("./booked");

module.exports = {
  ...events,
  ...user,
  ...booking
};
